package com.demo.springboot.service.serviceImpl;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.MovieInterface;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class MovieService implements MovieInterface {

    private MovieListDto movies;
    List<MovieDto> moviesList = new ArrayList<>();

    public MovieService() {

        moviesList.add(new MovieDto(1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        /*moviesList.add(new MovieDto(2,
                "John Rambo",
                1997,
                "https://fwfsdfdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );*/
        movies = new MovieListDto(moviesList);
    }

    @Override
    public MovieListDto getMovies() {
        moviesList.sort((a, b) -> b.getMovieId().compareTo(a.getMovieId()));
        movies = new MovieListDto(moviesList);
        return movies;
    }

    @Override
    public boolean createMovie(CreateMovieDto createMovieDto) {
        if (createMovieDto.getTitle() != null || createMovieDto.getYear() != null || createMovieDto.getImage() != null) {
            moviesList.add(new MovieDto(movies.getMovies().size() + 1, createMovieDto.getTitle(), createMovieDto.getYear(), createMovieDto.getImage()));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteMovie(Integer id) {

        for (MovieDto movieDto : movies.getMovies()) {
            if (movieDto.getMovieId().equals(id)) {
                movies.getMovies().remove(movieDto);
                return true;
            }
        }return false;
    }

    @Override
    public boolean changeMovie(Integer id,MovieDto movieDto){
        for (MovieDto movieDto1: movies.getMovies()){
            if (movieDto1.getMovieId().equals(id)) {
                movieDto1.setTitle(movieDto.getTitle());
                movieDto1.setYear(movieDto.getYear());
                movieDto1.setImage(movieDto.getImage());
                return true;
            }
            }
            return false;

    }
}
