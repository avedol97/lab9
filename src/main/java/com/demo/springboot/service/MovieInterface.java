package com.demo.springboot.service;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;

import java.util.List;

public interface MovieInterface {

    MovieListDto getMovies();
    boolean createMovie(CreateMovieDto createMovieDto);
    boolean deleteMovie(Integer id);
    boolean changeMovie(Integer id,MovieDto movieDto);
}
