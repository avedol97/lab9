package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.serviceImpl.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);


    @Autowired
    MovieService movieService;

    public MovieApiController(MovieService movieService) {
    }

    @GetMapping("/api/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        return ResponseEntity.ok().body(movieService.getMovies());
    }

    @PostMapping("/api/movies")
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto createMovieDto) throws URISyntaxException {
        movieService.createMovie(createMovieDto);
        return ResponseEntity.created(new URI("api/movies" +"2")).build();
    }

    @DeleteMapping("/api/movies/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable("id")Integer id){
        if (movieService.deleteMovie(id)) {
            return ResponseEntity.ok().build();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }


    @PutMapping("/api/movies/{id}")
    public ResponseEntity<Void> changeMovie(@PathVariable("id")Integer id,@RequestBody MovieDto movieDto){
    if(movieService.changeMovie(id,movieDto)){
        return ResponseEntity.ok().build();
    }else {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    }
}
